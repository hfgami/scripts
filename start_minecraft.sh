#!/bin/bash

# This script changes your minecraft username temporarly. It is useful if you want to play together over a LAN using the same minecraft account. Sometimes you will need to run it twice because the launcher will decide to "check in" and update your username. Replace "UsualName" with your minecraft account name. Make sure the replacement name is short and doesn't contain illegal characters. 

echo "Changing name to CaptAmerica"

cat ~/.minecraft/launcher_profiles.json | sed 's/UsualName/CaptAmerica/g' > ~/.minecraft/launcher_profiles.json.temp

cp ~/.minecraft/launcher_profiles.json.temp ~/.minecraft/launcher_profiles.json

echo "Done!"

echo "Starting Minecraft!"

/opt/minecraft-launcher/minecraft-launcher

