#!/usr/bin/env bash
### ### ### ### ### ### ###
###
### TO USE: 
###  1. Download this file to ~/Downloads
###  2. Open a terminal
###  3. Type these commands (without the $)
###  $ cd ~/Downloads
###  $ chmod +x fullbuild-wfview.sh
###  $ ./fullbuild-wfview.sh
###  
### ### ### ### ### ### ###
branch="unset"
if (($# < 1))
then
	branch="master"
else
	branch=$1
fi
echo "Using source code branch $branch"
echo ""
echo "This script will download dependencies, build, and install wfview. "
echo "It is designed for debian-based systems and "
echo "makes use of the apt command to satisfy dependencies. "
echo
echo "If it has been a while since this script was downloaded,"
echo "or if there are build errors, please use this command to"
echo "download a newer version of this script:"
echo
echo "wget https://gitlab.com/eliggett/scripts/-/raw/master/fullbuild-wfview.sh?inline=false -O fullbuild-wfview.sh; chmod 755 fullbuild-wfview.sh"
echo
echo "The 'sudo' command is used to run some commands as root."
echo "It (the sudo command) will ask for your password during this process."
echo "You should look at the source of this script if you have any doubts."
echo 
echo "Do you wish to install dependencies first? "
echo "If this is your first time building wfview,"
echo "or, if you have not done so in a while,"
echo "please select 'y', otherwise, press 'n'."
echo "If you are not sure, select 'y' to be safe."
echo
read -p "Press Y to install dependencies (Y/n): " -n 1 -r
echo   
if [[ $REPLY =~ ^[Yy]$ ]]
then
    E=0;
    sudo apt-get -y install build-essential || exit -1 
    sudo apt-get -y install qt5-qmake || exit -1
    sudo apt-get -y install libqt5core5a || exit -1 
    sudo apt-get -y install qtbase5-dev || exit -1 
    sudo apt-get -y install libqt5serialport5 libqt5serialport5-dev || exit -1
    sudo apt-get -y install libqt5multimedia5 || exit -1
    sudo apt-get -y install libqt5multimedia5-plugins || exit -1
    sudo apt-get -y install qtmultimedia5-dev || exit -1
    sudo apt-get -y install libopus-dev || exit -1
    sudo apt-get -y install libeigen3-dev || exit -1
    sudo apt-get -y install libportaudio2 libportaudiocpp0 || exit -1
    sudo apt-get -y install portaudio19-dev || exit -1
    sudo apt-get -y install librtaudio-dev librtaudio6 || exit -1
    sudo apt-get -y install git || exit -1

    echo "Almost done. Now we will install libqcustomplot."
    echo "One of these two commands will fail, which is ok. "
    echo "Only one of the next two commands need to work."

    sudo apt-get -y install libqcustomplot1.3 libqcustomplot-doc libqcustomplot-dev || ((E=E+1))
    sudo apt-get -y install libqcustomplot2.0 libqcustomplot-doc libqcustomplot-dev || ((E=E+1))

    if [ "$E" -eq 2 ]; then
        echo "Neither version of qcustomplot could be installed."
        echo "One of the two supported versions (1.3 or 2.0) must be installed to continue."
        exit -1
    else
        echo "Installing the required qcustomplot was successful."
    fi

    echo "Done installing dependencies."
else
    echo "Skipping dependency install steps. "
fi

read -p "Press enter to download wfview's source code."

SRCDIR=wfview--`date +%Y%m%d--%H-%M-%S`

echo "Now downloading the latest source code from the branch $branch"
echo "You can specify an alternate branch name as the argument to this script if you wish."
echo "The files will be downloaded into a directory named: $SRCDIR"

mkdir $SRCDIR

if [ $? -ne 0 ]; then
    echo "Error making download directory."
    exit -1
fi

cd $SRCDIR

### BRANCH SELECTION ###
# You may specify alternate branches using the first argument to this script
# example:
# ./fullbuild-wfview.sh audio-fixes
# the master branch is selected by default. 
# see here for a list of branches: https://gitlab.com/eliggett/wfview/-/branches
git clone --depth 1 https://gitlab.com/eliggett/wfview.git -b $branch

if [ $? -ne 0 ]; then
    echo "Error cloning source."
    echo "The git clone command failed to download wfview. "
    exit -1
fi

cd wfview

git submodule init

if [ $? -ne 0 ]; then
    echo "Error initializing submodules."
    echo "The git submodule init command failed. "
    exit -1
fi

git submodule update

if [ $? -ne 0 ]; then
    echo "Error updating submodules."
    echo "The git submodule update command failed. "
    exit -1
fi

cd ..

echo "Creating build directory 'build':"

mkdir build

if [ $? -ne 0 ]; then
    echo "Error making build directory."
    exit -1
fi


cd build
echo "The build process may take a few minutes."
read -p "Press enter to start. "

echo "Starting build process."

qmake ../wfview/wfview.pro

if [ $? -ne 0 ]; then
    echo "Error in qmake step."
    exit -1
fi

# Here we are taking a safe route using -j2. 
# For more robust systems, simply use -j without a number to use all available cores at once. 

time make -j2

if [ $? -ne 0 ]; then
    echo "Error in make step."
    echo "wfview was not compiled."
    echo "Please consider posting the error(s) to https://forum.wfview.org/"
    exit -1
fi

echo
read -p "Press Y to install wfview into your system (Y/n): " -n 1 -r
echo   

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Now installing: "
    sudo make install
    echo "Done running script."
else
    echo "Did not install. Program may be manually installed by running \"sudo make install\" from the build folder."
fi

