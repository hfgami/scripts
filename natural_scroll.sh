#!/bin/bash

# Add hardware names from xinput --list

xinput set-prop "A4Tech USB Optical Mouse" "libinput Natural Scrolling Enabled" 1
xinput set-prop "Logitech USB Mouse" "libinput Natural Scrolling Enabled" 1
xinput set-prop "Logitech USB Optical Mouse" "libinput Natural Scrolling Enabled" 1
#xinput set-prop pointer:"GTech Wireless Keypad  &  Mouse Receiver" 285 1
xinput set-prop pointer:"MOSART Semi. 2.4G Keyboard Mouse" "libinput Natural Scrolling Enabled" 1
